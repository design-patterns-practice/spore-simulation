from dataclasses import dataclass
from typing import Callable, Iterable, Optional, Tuple

from creatures import CreatureView
from movement.movement import Movement

# A function that receives a creature and outputs whether it satisfies a given condition.
Condition = Callable[[CreatureView], bool]


@dataclass(frozen=True)
class MovementType:
    condition: Condition
    movement: Movement


class BasicMovement:
    CRAWL = MovementType(
        condition=lambda creature: creature.stamina > 0,
        movement=Movement(stamina_consumed=1, position_change=1),
    )
    HOP = MovementType(
        condition=lambda creature: creature.legs >= 1 and creature.stamina > 20,
        movement=Movement(stamina_consumed=2, position_change=3),
    )
    WALK = MovementType(
        condition=lambda creature: creature.legs >= 2 and creature.stamina > 40,
        movement=Movement(stamina_consumed=2, position_change=4),
    )
    RUN = MovementType(
        condition=lambda creature: creature.legs >= 2 and creature.stamina > 60,
        movement=Movement(stamina_consumed=4, position_change=6),
    )
    FLY = MovementType(
        condition=lambda creature: creature.wings >= 2 and creature.stamina > 80,
        movement=Movement(stamina_consumed=4, position_change=8),
    )

    MOVEMENTS = (CRAWL, HOP, WALK, RUN, FLY)


class MinimalMovementLogic:
    @staticmethod
    def get_movement(creature: CreatureView) -> Optional[Movement]:
        """Returns a basic movement if the creature has any positive stamina, otherwise zero movement."""
        return (
            BasicMovement.CRAWL.movement
            if BasicMovement.CRAWL.condition(creature)  # type: ignore
            else None
        )

    def __repr__(self) -> str:
        return "Minimal Movement Logic."


OrderingFunction = Callable[[Iterable[MovementType]], Iterable[MovementType]]


class OrderedMovementLogic:
    def __init__(
        self,
        movement_types: Iterable[MovementType],
        ordering: OrderingFunction = lambda movement_types: movement_types,
        reorder_before_movement: bool = False,
    ) -> None:
        self.__movement_types = movement_types
        self.__ordering = ordering
        self.__reorder_before_movement = reorder_before_movement

        if not self.__reorder_before_movement:
            self.__movement_types = self.__ordering(movement_types)

    def get_movement(self, creature: CreatureView) -> Optional[Movement]:
        """Returns a locally optimal movement based on the resources of the given creature."""

        movement_types = (
            self.__ordering(self.__movement_types)
            if self.__reorder_before_movement
            else self.__movement_types
        )

        movement = next(
            (
                movement_type.movement
                for movement_type in movement_types
                if movement_type.condition(creature)  # type: ignore
            ),
            None,
        )

        return movement


class FastMovementLogic(OrderedMovementLogic):
    def __init__(self, movement_types: Iterable[MovementType]):
        super().__init__(
            movement_types=movement_types,
            ordering=FastMovementLogic.__ordering,
            reorder_before_movement=False,
        )

    def __repr__(self) -> str:
        return "Fast Movement Logic."

    @staticmethod
    def __ordering(movement_types: Iterable[MovementType]) -> Iterable[MovementType]:
        """Orders the specified iterable of movement types according to their speed of movement."""

        def key_function(movement_type: MovementType) -> Tuple[int, int]:
            position_change = movement_type.movement.position_change
            stamina_consumed = movement_type.movement.stamina_consumed

            return position_change, stamina_consumed

        return sorted(movement_types, key=key_function, reverse=True)


class EfficientMovementLogic(OrderedMovementLogic):
    def __init__(self, movement_types: Iterable[MovementType]):
        super().__init__(
            movement_types=movement_types,
            ordering=EfficientMovementLogic.__ordering,
            reorder_before_movement=False,
        )

    def __repr__(self) -> str:
        return "Efficient Movement Logic."

    @staticmethod
    def __ordering(movement_types: Iterable[MovementType]) -> Iterable[MovementType]:
        """Orders the specified iterable of movement types according to their efficiency (speed/stamina) of movement."""

        def key_function(movement_type: MovementType) -> float:
            return movement_type.movement.efficiency

        return sorted(movement_types, key=key_function, reverse=True)
