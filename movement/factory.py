from random import choice
from typing import Protocol

from movement.basic import (
    BasicMovement,
    EfficientMovementLogic,
    FastMovementLogic,
    MinimalMovementLogic,
)
from movement.movement import IMovementLogic


class IMovementLogicFactory(Protocol):
    def generate_logic(self) -> IMovementLogic:
        """Generates and returns a movement logic for a creature."""


class DefaultMovementLogicFactory:
    @staticmethod
    def generate_logic() -> IMovementLogic:
        """Generates and returns a movement logic for a creature."""
        return MinimalMovementLogic()


class RandomMovementLogicFactory:
    @staticmethod
    def generate_logic() -> IMovementLogic:
        """Generates and returns a movement logic for a creature."""
        return choice(  # type: ignore
            [
                MinimalMovementLogic(),
                FastMovementLogic(BasicMovement.MOVEMENTS),
                EfficientMovementLogic(BasicMovement.MOVEMENTS),
            ]
        )
