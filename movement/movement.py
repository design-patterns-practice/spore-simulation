from dataclasses import dataclass
from functools import cached_property
from typing import Optional, Protocol

from creatures import CreatureView


@dataclass(frozen=True)
class Movement:
    stamina_consumed: int
    position_change: int

    @cached_property
    def efficiency(self) -> float:
        """Returns the efficiency (i.e. delta(position)/delta(stamina)) of this movement."""
        if self.position_change == 0:
            return 0
        if self.stamina_consumed == 0:
            return float("inf")

        return self.position_change / self.stamina_consumed


class IMovementLogic(Protocol):
    def get_movement(self, creature: CreatureView) -> Optional[Movement]:
        """Returns The change of position according to this movement logic for the given creature.
        If the creature cannot move in it's current state, the logic should return none."""
