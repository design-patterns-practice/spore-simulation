from typing import Callable

from hypothesis.strategies import (
    SearchStrategy,
    builds,
    integers,
    lists,
    one_of,
    sampled_from,
)

from creatures import (
    Creature,
    CreatureView,
    DefaultCreatureFactory,
    ICreatureFactory,
    RandomCreatureFactory,
)
from movement import (
    BasicMovement,
    DefaultMovementLogicFactory,
    EfficientMovementLogic,
    FastMovementLogic,
    IMovementLogic,
    MinimalMovementLogic,
    RandomMovementLogicFactory,
)
from movement.basic import MovementType
from worlds import (
    DefaultPositionGenerator,
    IPositionGenerator,
    IWorld,
    RandomPositionGenerator,
    World,
)

EvolveCall = Callable[[IWorld], CreatureView]


def worlds() -> SearchStrategy[World]:
    """Returns worlds with all possible configurations."""
    return builds(
        lambda creature_factory_supplier, position_generator_supplier, movement_logic_factory_supplier: World(
            creature_factory=creature_factory_supplier(),
            creature_position_generator=position_generator_supplier(),
            movement_logic_factory=movement_logic_factory_supplier(),
        ),
        creature_factory_supplier=sampled_from(
            [DefaultCreatureFactory, RandomCreatureFactory]
        ),
        position_generator_supplier=sampled_from(
            [DefaultPositionGenerator, RandomPositionGenerator]
        ),
        movement_logic_factory_supplier=sampled_from(
            [DefaultMovementLogicFactory, RandomMovementLogicFactory]
        ),
    )


def evolve_calls() -> SearchStrategy[EvolveCall]:
    """Returns functions that call both evolve methods from the IWorld interface."""
    return sampled_from(
        [lambda world: world.evolve_predator(), lambda world: world.evolve_pray()]
    )


def random_creature_factories() -> SearchStrategy[RandomCreatureFactory]:
    """Returns random creature factories."""
    return builds(
        RandomCreatureFactory,
        max_power=integers(min_value=0),
        max_health=integers(min_value=0),
        max_stamina=integers(min_value=0),
    )


def creature_factories() -> SearchStrategy[ICreatureFactory]:
    """Returns all existing creature factories."""
    return one_of(sampled_from([DefaultCreatureFactory()]), random_creature_factories())


def creatures() -> SearchStrategy[Creature]:
    """Returns creatures."""

    def creature_from(factory: ICreatureFactory) -> Creature:
        return factory.evolve_creature(0)

    return builds(
        creature_from,
        factory=creature_factories(),
    )


def position_generators() -> SearchStrategy[IPositionGenerator]:
    """Returns position generators."""
    return sampled_from([DefaultPositionGenerator(), RandomPositionGenerator(0, 100)])


def random_position_generators() -> SearchStrategy[RandomPositionGenerator]:
    """Returns random position generators."""
    return builds(
        lambda bounds: RandomPositionGenerator(
            lower_bound=bounds[0], upper_bound=bounds[1]
        ),
        bounds=lists(integers(), min_size=2, max_size=2).filter(
            lambda bounds: bounds[0] < bounds[1]
        ),
    )


def movement_logics() -> SearchStrategy[IMovementLogic]:
    """Returns movement logics."""
    return sampled_from(
        [
            MinimalMovementLogic(),
            FastMovementLogic(BasicMovement.MOVEMENTS),
            EfficientMovementLogic(BasicMovement.MOVEMENTS),
        ]
    )


def basic_movement_types() -> SearchStrategy[MovementType]:
    """Returns basic movement types."""
    return sampled_from(BasicMovement.MOVEMENTS)
