from typing import Optional

from hypothesis import assume, given

from creatures import Creature
from movement import (
    EfficientMovementLogic,
    FastMovementLogic,
    IMovementLogic,
    Movement,
    MovementType,
)
from tests.strategies import basic_movement_types, creatures, movement_logics


@given(
    creature=creatures().filter(lambda creature: creature.stamina == 0),
    movement_logic=movement_logics(),
)
def test_should_not_move_creature(
    creature: Creature, movement_logic: IMovementLogic
) -> None:
    assert movement_logic.get_movement(creature.view) is None


@given(
    creature=creatures().filter(lambda creature: creature.stamina > 0),
    movement_logic=movement_logics(),
)
def test_should_move_creature(
    creature: Creature, movement_logic: IMovementLogic
) -> None:

    movement: Optional[Movement] = movement_logic.get_movement(creature.view)

    assert movement is not None

    assert movement.position_change > 0
    assert movement.stamina_consumed > 0
    assert movement.stamina_consumed <= creature.stamina


@given(creature=creatures(), basic_movement_type=basic_movement_types())
def test_should_move_creature_the_fastest(
    creature: Creature,
    basic_movement_type: MovementType,
    fast_movement_logic: FastMovementLogic,
) -> None:
    assume(basic_movement_type.condition(creature.view))  # type: ignore

    movement: Optional[Movement] = fast_movement_logic.get_movement(creature.view)

    assert movement is not None

    assert movement.position_change >= basic_movement_type.movement.position_change
    assert movement.stamina_consumed <= creature.stamina


@given(creature=creatures(), basic_movement_type=basic_movement_types())
def test_should_move_creature_most_efficiently(
    creature: Creature,
    basic_movement_type: MovementType,
    efficient_movement_logic: EfficientMovementLogic,
) -> None:
    assume(basic_movement_type.condition(creature.view))  # type: ignore

    movement: Optional[Movement] = efficient_movement_logic.get_movement(creature.view)

    assert movement is not None

    assert movement.efficiency >= basic_movement_type.movement.efficiency
    assert movement.stamina_consumed <= creature.stamina
