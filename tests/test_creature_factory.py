from hypothesis import given

from creatures import Creature, ICreatureFactory
from tests.strategies import creature_factories


@given(factory=creature_factories())
def test_should_create_creature(factory: ICreatureFactory) -> None:
    test_creature: Creature = factory.evolve_creature(0)

    assert test_creature is not None

    assert test_creature.position == 0
    assert test_creature.power >= 0
    assert test_creature.health >= 0
    assert test_creature.stamina >= 0
