from hypothesis import assume, given

from loggers import LoggedWorld
from mocks import MockChaseLogger, MockCreatureLogger
from tests.conftest import Supplier
from tests.strategies import EvolveCall, evolve_calls, worlds
from worlds import IWorld


@given(world=worlds(), evolve_call=evolve_calls())
def test_should_log_evolved_creature(
    world: IWorld,
    evolve_call: EvolveCall,
    mock_creature_logger_supplier: Supplier[MockCreatureLogger],
) -> None:

    logger = mock_creature_logger_supplier()

    evolved_creature = evolve_call(
        LoggedWorld(
            world,
            predator_logger=logger,
            pray_logger=logger,
        )
    )

    assert len(logger.logged_items) == 1
    assert logger.logged_items[0] is evolved_creature


@given(world=worlds())
def test_should_log_chase_result(
    world: IWorld, mock_chase_logger_supplier: Supplier[MockChaseLogger]
) -> None:

    logger = mock_chase_logger_supplier()

    world.evolve_predator()
    world.evolve_pray()

    result: bool = LoggedWorld(world, chase_logger=logger).simulate_chase()

    assert len(logger.logged_items) == 1
    assert logger.logged_items[0] is result


@given(world=worlds())
def test_should_log_fight_result(
    world: IWorld, mock_fight_logger_supplier: Supplier[MockChaseLogger]
) -> None:

    logger = mock_fight_logger_supplier()

    world.evolve_predator()
    world.evolve_pray()

    assume(world.simulate_chase() is True)

    result: bool = LoggedWorld(world, fight_logger=logger).simulate_fight()

    assert len(logger.logged_items) == 1
    assert logger.logged_items[0] is result
