from hypothesis import assume, given

from creatures import CreatureView
from tests.strategies import EvolveCall, evolve_calls, worlds
from worlds import IWorld


@given(world=worlds())
def test_should_create_world(world: IWorld) -> None:
    assert world is not None


@given(world=worlds(), evolve_call=evolve_calls())
def test_should_evolve_creature(world: IWorld, evolve_call: EvolveCall) -> None:
    test_creature: CreatureView = evolve_call(world)

    assert test_creature is not None

    assert test_creature.position >= 0
    assert test_creature.power >= 0
    assert test_creature.health >= 0
    assert test_creature.stamina >= 0


@given(world=worlds())
def test_should_simulate_chase(world: IWorld) -> None:
    world.evolve_predator()
    world.evolve_pray()

    assert world.simulate_chase() in {True, False}


@given(world=worlds())
def test_should_result_in_fight(world: IWorld) -> None:
    predator_view: CreatureView = world.evolve_predator()
    pray_view: CreatureView = world.evolve_pray()

    assume(world.simulate_chase() is True)
    assert predator_view.position >= pray_view.position


@given(world=worlds())
def test_should_result_in_pray_running_away(world: IWorld) -> None:
    predator_view: CreatureView = world.evolve_predator()
    pray_view: CreatureView = world.evolve_pray()

    assume(world.simulate_chase() is False)
    assert predator_view.position < pray_view.position


@given(world=worlds())
def test_should_simulate_fight(world: IWorld) -> None:
    world.evolve_predator()
    world.evolve_pray()

    assume(world.simulate_chase() is True)
    assert world.simulate_fight() in {True, False}


@given(world=worlds())
def test_should_result_in_defeated_pray(world: IWorld) -> None:
    predator_view: CreatureView = world.evolve_predator()
    pray_view: CreatureView = world.evolve_pray()

    assume(world.simulate_chase() is True)
    assume(world.simulate_fight() is True)

    assert predator_view.health > 0 and pray_view.health == 0


@given(world=worlds())
def test_should_result_in_defeated_predator(world: IWorld) -> None:
    predator_view: CreatureView = world.evolve_predator()
    pray_view: CreatureView = world.evolve_pray()

    assume(world.simulate_chase() is True)
    assume(world.simulate_fight() is False)

    assert predator_view.health == 0 and pray_view.health >= 0
