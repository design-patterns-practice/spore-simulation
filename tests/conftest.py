from typing import Callable, TypeVar

import pytest

from mocks import MockChaseLogger, MockCreatureLogger, MockFightLogger, MockLogger
from movement import BasicMovement, EfficientMovementLogic, FastMovementLogic

T = TypeVar("T")
Supplier = Callable[[], T]


@pytest.fixture(scope="module")
def fast_movement_logic() -> FastMovementLogic:
    return FastMovementLogic(BasicMovement.MOVEMENTS)


@pytest.fixture(scope="module")
def efficient_movement_logic() -> EfficientMovementLogic:
    return EfficientMovementLogic(BasicMovement.MOVEMENTS)


@pytest.fixture(scope="module")
def mock_creature_logger_supplier() -> Supplier[MockCreatureLogger]:
    return lambda: MockCreatureLogger(MockLogger())


@pytest.fixture(scope="module")
def mock_chase_logger_supplier() -> Supplier[MockChaseLogger]:
    return lambda: MockChaseLogger(MockLogger())


@pytest.fixture(scope="module")
def mock_fight_logger_supplier() -> Supplier[MockFightLogger]:
    return lambda: MockFightLogger(MockLogger())
