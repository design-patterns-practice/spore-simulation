from hypothesis import example, given

from tests.strategies import position_generators, random_position_generators
from worlds import IPositionGenerator, RandomPositionGenerator


@given(position_generator=position_generators())
def test_should_generate_non_negative_position(
    position_generator: IPositionGenerator,
) -> None:
    assert position_generator.generate_predator_position() >= 0
    assert position_generator.generate_pray_position() >= 0


@given(random_position_generator=random_position_generators())
@example(random_position_generator=RandomPositionGenerator(-54, 1))
def test_should_evolve_pray_to_the_right_of_predator(
    random_position_generator: RandomPositionGenerator,
) -> None:

    predator_position = random_position_generator.generate_predator_position()
    pray_position = random_position_generator.generate_pray_position()

    assert predator_position < pray_position
