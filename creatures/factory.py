from dataclasses import dataclass
from random import choice, randint
from typing import Protocol, Sequence

from creatures import Claws, Creature, Teeth


class ICreatureFactory(Protocol):
    def evolve_creature(self, position: int) -> Creature:
        """Evolves a creature at the specified position."""


class DefaultCreatureFactory:
    @staticmethod
    def evolve_creature(position: int) -> Creature:
        """Evolves a default creature at the specified position."""
        return Creature(position=position)


@dataclass(frozen=True)
class RandomCreatureFactory:
    max_power: int = 200
    max_health: int = 1000
    max_stamina: int = 1000
    max_legs: int = 20
    max_wings: int = 8
    possible_claws: Sequence[Claws] = (Claws.SMALL, Claws.MEDIUM, Claws.BIG)
    possible_teeth: Sequence[Teeth] = (Teeth.DULL, Teeth.MODERATE, Teeth.SHARP)

    def evolve_creature(self, position: int) -> Creature:
        """Evolves a random creature at the specified position."""

        def random(upper_bound: int) -> int:
            return randint(0, upper_bound)

        return Creature(
            position=position,
            power=random(self.max_power),
            health=random(self.max_health),
            stamina=random(self.max_stamina),
            legs=random(self.max_legs),
            wings=random(self.max_wings),
            claws=choice(self.possible_claws),
            teeth=choice(self.possible_teeth),
        )
