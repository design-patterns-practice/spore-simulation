from dataclasses import dataclass
from enum import Enum
from functools import cached_property
from typing import Optional, Type, final


class Claws(Enum):
    SMALL = 2
    MEDIUM = 3
    BIG = 4


class Teeth(Enum):
    DULL = 3
    MODERATE = 6
    SHARP = 9


@final
class _CreateKey:
    pass


@dataclass
class Creature:
    position: int
    power: int = 1
    health: int = 100
    stamina: int = 100
    legs: int = 0
    wings: int = 0
    claws: Optional[Claws] = None
    teeth: Optional[Teeth] = None

    def __post_init__(self) -> None:
        self.__view = CreatureView(_CreateKey, self)

    @property
    def view(self) -> "CreatureView":
        return self.__view

    @cached_property
    def attack_power(self) -> int:

        power = self.power
        if self.claws is not None:
            power *= self.claws.value

        if self.teeth is not None:
            power += self.teeth.value

        return power


class CreatureView:
    def __init__(self, create_key: Type[_CreateKey], creature: Creature):
        assert (
            create_key == _CreateKey
        ), "A creature view should only be retrieved by invoking .view() on a Creature."

        self.__creature = creature

    def __repr__(self) -> str:
        return (
            f"Creature(\n"
            f"\tPosition={self.position}\n"
            f"\tPower={self.power}\n"
            f"\tHealth={self.health}\n"
            f"\tStamina={self.stamina}\n"
            f"\tLegs={self.legs}\n"
            f"\tWings={self.wings}\n"
            f"\tClaws={self.claws}\n"
            f"\tTeeth={self.teeth}\n"
            f")"
        )

    @property
    def position(self) -> int:
        return self.__creature.position

    @property
    def power(self) -> int:
        return self.__creature.power

    @property
    def health(self) -> int:
        return self.__creature.health

    @property
    def stamina(self) -> int:
        return self.__creature.stamina

    @property
    def legs(self) -> int:
        return self.__creature.legs

    @property
    def wings(self) -> int:
        return self.__creature.wings

    @property
    def claws(self) -> Optional[Claws]:
        return self.__creature.claws

    @property
    def teeth(self) -> Optional[Teeth]:
        return self.__creature.teeth
