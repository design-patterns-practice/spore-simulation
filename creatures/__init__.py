from creatures.creature import Claws, Creature, CreatureView, Teeth
from creatures.factory import (
    DefaultCreatureFactory,
    ICreatureFactory,
    RandomCreatureFactory,
)
