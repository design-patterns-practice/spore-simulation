from dataclasses import dataclass
from typing import Optional, Protocol

from creatures import Creature, CreatureView, DefaultCreatureFactory, ICreatureFactory
from movement import (
    DefaultMovementLogicFactory,
    IMovementLogic,
    IMovementLogicFactory,
    Movement,
)
from worlds import DefaultPositionGenerator, IPositionGenerator


class IWorld(Protocol):
    def evolve_predator(self) -> CreatureView:
        """Evolves a predator at some location in this world."""

    def evolve_pray(self) -> CreatureView:
        """Evolves a pray at some location in this world."""

    def simulate_chase(self) -> bool:
        """Simulates a chase between the predator and pray.

        :returns True if the predator caught up to the pray, false otherwise."""

    def simulate_fight(self) -> bool:
        """Simulates a fight between the predator and pray.

        :returns True if the predator defeated the pray, false otherwise."""


class World:
    def __init__(
        self,
        creature_factory: Optional[ICreatureFactory] = None,
        creature_position_generator: Optional[IPositionGenerator] = None,
        movement_logic_factory: Optional[IMovementLogicFactory] = None,
    ):
        if creature_factory is None:
            creature_factory = self.__default_creature_factory()

        if creature_position_generator is None:
            creature_position_generator = self.__default_position_generator()

        if movement_logic_factory is None:
            movement_logic_factory = self.__default_movement_logic_factory()

        self.__creature_factory: ICreatureFactory = creature_factory
        self.__position_generator: IPositionGenerator = creature_position_generator
        self.__movement_logic_factory: IMovementLogicFactory = movement_logic_factory
        self.__predator: Optional[Creature] = None
        self.__pray: Optional[Creature] = None
        self.__chase_complete: bool = False

    def __repr__(self) -> str:
        return f"World(Creature Factory={self.__creature_factory}, Position Generator={self.__position_generator})"

    def evolve_predator(self) -> CreatureView:
        """Evolves a predator at a random location in this world."""
        position = self.__position_generator.generate_predator_position()
        self.__predator = self.__creature_factory.evolve_creature(position)
        return self.__predator.view

    def evolve_pray(self) -> CreatureView:
        """Evolves a pray at a random location in this world."""
        position = self.__position_generator.generate_pray_position()
        self.__pray = self.__creature_factory.evolve_creature(position)
        return self.__pray.view

    def simulate_chase(self) -> bool:
        """Simulates a chase between the predator and pray.

        :returns True if the predator caught up to the pray, false otherwise."""
        assert self.__predator, "A predator has not yet been evolved."
        assert self.__pray, "A pray has not yet been evolved."
        assert not self.__chase_complete, "A chase has already been simulated."

        predator_movement_logic = self.__movement_logic_factory.generate_logic()
        pray_movement_logic = self.__movement_logic_factory.generate_logic()

        while self.__predator.position < self.__pray.position:
            predator_moved = World.__move(self.__predator, predator_movement_logic)
            pray_moved = World.__move(self.__pray, pray_movement_logic)

            if not predator_moved and not pray_moved:
                break

        self.__chase_complete = True

        return self.__predator.position >= self.__pray.position

    def simulate_fight(self) -> bool:
        """Simulates a fight between the predator and pray.

        :returns True if the predator defeated the pray, false otherwise."""
        assert self.__predator, "A predator has not yet been evolved."
        assert self.__pray, "A pray has not yet been evolved."
        assert self.__chase_complete, "A chase must be simulated before a fight."
        assert (
            self.__predator.position >= self.__pray.position
        ), "A fight cannot be simulated if the predator has not caught the pray"

        while self.__predator.health > 0 and self.__pray.health > 0:
            self.__pray.health -= self.__predator.attack_power
            self.__predator.health -= self.__pray.attack_power

        self.__predator.health = max(self.__predator.health, 0)
        self.__pray.health = max(self.__pray.health, 0)

        return self.__predator.health > 0

    @staticmethod
    def __move(creature: Creature, movement_logic: IMovementLogic) -> bool:
        """Moves the specified creature, according to the given movement logic once.

        :returns True if the creature successfully moved, false otherwise."""
        movement: Optional[Movement] = movement_logic.get_movement(creature.view)

        if movement is None:
            return False

        creature.position += movement.position_change
        creature.stamina -= movement.stamina_consumed

        return True

    @staticmethod
    def __attack() -> None:
        pass

    @staticmethod
    def __default_creature_factory() -> ICreatureFactory:
        """Returns an instance of the default creature factory."""
        return DefaultCreatureFactory()

    @staticmethod
    def __default_position_generator() -> IPositionGenerator:
        """Returns an instance of the default position generator."""
        return DefaultPositionGenerator()

    @staticmethod
    def __default_movement_logic_factory() -> IMovementLogicFactory:
        """Returns an instance of the default movement logic factory."""
        return DefaultMovementLogicFactory()


@dataclass(frozen=True)
class WorldDecorator:
    world: IWorld

    def evolve_predator(self) -> CreatureView:
        """Evolves a predator at some location in this world."""

    def evolve_pray(self) -> CreatureView:
        """Evolves a pray at some location in this world."""

    def simulate_chase(self) -> bool:
        """Simulates a chase between the predator and pray.

        :returns True if the predator caught up to the pray, false otherwise."""

    def simulate_fight(self) -> bool:
        """Simulates a fight between the predator and pray.

        :returns True if the predator defeated the pray, false otherwise."""
