import sys
from random import randint
from typing import Optional, Protocol


class IPositionGenerator(Protocol):
    def generate_predator_position(self) -> int:
        """Generates a position for a predator."""

    def generate_pray_position(self) -> int:
        """Generates a position for a pray."""


class DefaultPositionGenerator:
    @staticmethod
    def generate_predator_position() -> int:
        """Generates a zero position for a predator."""
        return 0

    @staticmethod
    def generate_pray_position() -> int:
        """Generates a zero position for a pray."""
        return 0


class RandomPositionGenerator:
    def __init__(self, lower_bound: int = 0, upper_bound: int = sys.maxsize):

        assert (
            lower_bound < upper_bound
        ), f"The specified range must be well-formed. Received: ({lower_bound}, {upper_bound})"

        self.__lower_bound = lower_bound
        self.__upper_bound = upper_bound
        self.__predator_position: Optional[int] = None
        self.__pray_position: Optional[int] = None

    def __repr__(self) -> str:
        return f"RandomPositionGenerator(Lower Bound={self.__lower_bound}, Upper Bound={self.__upper_bound})"

    def generate_predator_position(self) -> int:
        """Generates a random position for a predator."""

        assert (
            self.__predator_position is None
        ), f"A position was already generated for the predator: {self.__predator_position}"

        if self.__pray_position is None:
            upper_bound = self.__upper_bound
        else:
            upper_bound = min(self.__pray_position, self.__upper_bound)

        self.__predator_position = randint(self.__lower_bound, upper_bound - 1)
        return self.__predator_position

    def generate_pray_position(self) -> int:
        """Generates a random position for a pray."""

        assert (
            self.__pray_position is None
        ), f"A position was already generated for the pray: {self.__pray_position}"

        if self.__predator_position is None:
            lower_bound = self.__lower_bound
        else:
            lower_bound = max(self.__predator_position, self.__lower_bound)

        self.__pray_position = randint(lower_bound + 1, self.__upper_bound)
        return self.__pray_position
