from creatures import RandomCreatureFactory
from loggers import LoggedWorld
from movement import RandomMovementLogicFactory
from worlds import IWorld, RandomPositionGenerator, World


def start_simulation() -> None:
    """Simulates a world with a single predator and pray."""

    for i in range(1, 101):

        print(f"Simulation #{i} Started.")

        world: IWorld = LoggedWorld(
            World(
                creature_factory=RandomCreatureFactory(),
                creature_position_generator=RandomPositionGenerator(0, 100),
                movement_logic_factory=RandomMovementLogicFactory(),
            )
        )

        world.evolve_predator()
        world.evolve_pray()
        if world.simulate_chase():
            world.simulate_fight()

        print("\nSimulation Finished.\n")


if __name__ == "__main__":
    start_simulation()
