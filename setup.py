from setuptools import setup

setup(
    name="Spore Simulation",
    version="1.0.0",
    description='A small CLI simulation inspired by the game "Spore".',
    author="Zurab Mujirishvili",
    author_email="zmuji18@freeuni.edu.ge",
)
