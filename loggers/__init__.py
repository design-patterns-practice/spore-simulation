from loggers.chase import DefaultChaseLogger, IChaseLogger
from loggers.creature import DefaultPrayLogger, DefaultPredatorLogger, ICreatureLogger
from loggers.fight import DefaultFightLogger, IFightLogger
from loggers.world import LoggedWorld
