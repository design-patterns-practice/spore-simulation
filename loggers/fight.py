from typing import Protocol


class IFightLogger(Protocol):
    def log_fight(self, pray_defeated: bool) -> None:
        """Logs the result of the chase.

        :parameter pray_defeated True if the predator has defeated the pray, otherwise false."""


class DefaultFightLogger:
    @staticmethod
    def log_fight(pray_defeated: bool) -> None:
        """Logs the result of the chase.

        :parameter pray_defeated True if the predator has defeated the pray, otherwise false."""

        if pray_defeated:
            print("Some R rated things have happened.")
        else:
            print("Pray ran into infinity.")
