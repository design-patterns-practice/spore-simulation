from typing import Protocol


class IChaseLogger(Protocol):
    def log_chase(self, pray_caught: bool) -> None:
        """Logs the result of the chase.

        :parameter pray_caught True if the predator has caught up with the pray, otherwise false."""


class DefaultChaseLogger:
    @staticmethod
    def log_chase(pray_caught: bool) -> None:
        """Logs the result of the chase.

        :parameter pray_caught True if the predator has caught up with the pray, otherwise false."""
        if pray_caught is False:
            print("Pray ran into infinity.")
