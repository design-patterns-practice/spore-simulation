from dataclasses import dataclass, field

from creatures import CreatureView
from loggers import (
    DefaultChaseLogger,
    DefaultFightLogger,
    DefaultPrayLogger,
    DefaultPredatorLogger,
    IChaseLogger,
    ICreatureLogger,
    IFightLogger,
)
from worlds import WorldDecorator


@dataclass(frozen=True)
class LoggedWorld(WorldDecorator):
    predator_logger: ICreatureLogger = field(default_factory=DefaultPredatorLogger)
    pray_logger: ICreatureLogger = field(default_factory=DefaultPrayLogger)
    chase_logger: IChaseLogger = field(default_factory=DefaultChaseLogger)
    fight_logger: IFightLogger = field(default_factory=DefaultFightLogger)

    def evolve_predator(self) -> CreatureView:
        """Evolves a predator at a random location in this world and logs it's characteristics."""
        creature: CreatureView = self.world.evolve_predator()
        self.predator_logger.log_creature(creature)
        return creature

    def evolve_pray(self) -> CreatureView:
        """Evolves a pray at a random location in this world and logs it's characteristics."""
        creature: CreatureView = self.world.evolve_pray()
        self.pray_logger.log_creature(creature)
        return creature

    def simulate_chase(self) -> bool:
        """Simulates a chase between the predator and pray.

        :returns True if the predator caught up to the pray, false otherwise."""
        pray_caught: bool = self.world.simulate_chase()
        self.chase_logger.log_chase(pray_caught)
        return pray_caught

    def simulate_fight(self) -> bool:
        """Simulates a fight between the predator and pray.

        :returns True if the predator defeated the pray, false otherwise."""
        pray_defeated: bool = self.world.simulate_fight()
        self.fight_logger.log_fight(pray_defeated)
        return pray_defeated
