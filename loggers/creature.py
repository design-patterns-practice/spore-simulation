from typing import Protocol

from creatures import CreatureView


class ICreatureLogger(Protocol):
    def log_creature(self, creature: CreatureView) -> None:
        """Logs the characteristics of the specified creature."""


class DefaultPredatorLogger:
    @staticmethod
    def log_creature(predator: CreatureView) -> None:
        """Logs the characteristics of the specified predator."""
        print(f"Evolved predator characteristics: {predator}")


class DefaultPrayLogger:
    @staticmethod
    def log_creature(pray: CreatureView) -> None:
        """Logs the characteristics of the specified predator."""
        print(f"Evolved pray characteristics: {pray}")
