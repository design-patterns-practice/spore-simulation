from typing import Generic, List, Tuple, TypeVar

from creatures import CreatureView

T = TypeVar("T")


class MockLogger(Generic[T]):
    def __init__(self) -> None:
        self.__logged_items: List[T] = []

    def log(self, logged_item: T) -> None:
        self.__logged_items.append(logged_item)

    @property
    def logged_items(self) -> Tuple[T, ...]:
        return tuple(self.__logged_items)


class MockCreatureLogger:
    def __init__(self, logger: MockLogger[CreatureView]) -> None:
        self.__logger = logger

    def log_creature(self, creature: CreatureView) -> None:
        return self.__logger.log(creature)

    @property
    def logged_items(self) -> Tuple[CreatureView, ...]:
        return self.__logger.logged_items


class MockChaseLogger:
    def __init__(self, logger: MockLogger[bool]) -> None:
        self.__logger = logger

    def log_chase(self, pray_caught: bool) -> None:
        return self.__logger.log(pray_caught)

    @property
    def logged_items(self) -> Tuple[bool, ...]:
        return self.__logger.logged_items


class MockFightLogger:
    def __init__(self, logger: MockLogger[bool]) -> None:
        self.__logger = logger

    def log_fight(self, pray_defeated: bool) -> None:
        return self.__logger.log(pray_defeated)

    @property
    def logged_items(self) -> Tuple[bool, ...]:
        return self.__logger.logged_items
